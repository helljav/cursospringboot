package com.bolsadeideas.springboot.horariointerceptor.app.interceptors;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

@Component("horario")
@Slf4j
public class HorarioInterceptor implements HandlerInterceptor {
	@Value("${config.horario.apertura}")
	private Integer apertura;
	
	@Value("${config.horario.cierre}")
	private Integer cierre;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		//Obtiene la hora del dispositivo en ese momento
		Calendar calendar = Calendar.getInstance();
		int hora = calendar.get(calendar.HOUR_OF_DAY);
		
		if(hora >= apertura && hora < cierre) {
			StringBuilder mensaje = new StringBuilder("Bienvenido al horario de atencion a clientes");
			mensaje.append(", atendemos desde las ");
			mensaje.append(apertura);
			mensaje.append(" hasta las ");
			mensaje.append(cierre);
			
			//Con esto pasamos la variable desde el interceptor a la vista
			request.setAttribute("mensaje", mensaje.toString());
			log.info("Si entre pai como la vez?");
			return true;
			
		}
		
		//Redireccionamos a la ruta en el controlador
		response.sendRedirect(request.getContextPath().concat("/cerrado"));
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
		//Obtenemos el mensaje y se lo pasamos a la vista
		String mensaje = (String) request.getAttribute("mensaje");
		if(modelAndView !=null && handler instanceof HandlerMethod) {
			modelAndView.addObject("horario", mensaje);
		}
		
		
	}

}
