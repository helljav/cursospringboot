package com.bolsadeideas.springboot.form.app.interceptors;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

/**
 * En esta clase se implementan los interceptores, normalmente utilizados en las peticiones htttp
 * Se utilizan para hacer cosas (Validaciones, operaciones, etc) que se ejecutan antes Y/o despues 
 * de un metodo en el controlador.
 * Si debuelve true, continua con los metodos en el controlador, si debuelve false para la operacion
 * @author HP
 *
 */
@Component("tiempoTranscurridoInterceptor")
@Slf4j
public class TiempoTranscurridoInterceptor implements HandlerInterceptor{
	
	

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		//Con esto hacemos que no se aplique en el metodo post ya que solo debolveria true
		if(request.getMethod().equalsIgnoreCase("post")) {
			return true;
		}
		
		log.info("Tiempor transcurrido: preHandle() entrando ...");
		long tiempoInicio = System.currentTimeMillis();
		request.setAttribute("timpoInicio", tiempoInicio);
		
		//Simula una carga a uestr request
		Random random = new Random();
		Integer demora = random.nextInt(500);
		Thread.sleep(demora);
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {		
		HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
		
		if(request.getMethod().equalsIgnoreCase("post")) {
			return;
		}
		
		long tiempoFin = System.currentTimeMillis();
		long tiempoInicio = (Long) request.getAttribute("timpoInicio");
		long tiempoTranscurrido = tiempoFin - tiempoInicio;
		
		
		if(modelAndView != null) {
			modelAndView.addObject("tiempoTranscurrido", tiempoTranscurrido);
		}
		log.info("Tiempor transcurrido: " + tiempoTranscurrido);
		log.info("Tiempor transcurrido: postHandle() saliendo ...");
	}
	

}
