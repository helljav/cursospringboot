package com.bolsadeideas.springboot.form.app.models.domain;

import lombok.Data;

//import javax.validation.constraints.NotEmpty;
@Data
public class Pais {

	private Integer id;
	//@NotEmpty
	private String codigo;
	private String nombre;

	public Pais() {
	}

	public Pais(Integer id, String codigo, String nombre) {
		this.id = id;
		this.codigo = codigo;
		this.nombre = nombre;
	}

	/*
	 * Este metodo nos permite poder leer el pais y que se quede seleccionado en automatico
	 * un pais en el la vista form
	 */
	@Override
	public String toString() {
		return this.id.toString();
	}

}
