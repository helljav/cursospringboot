package com.bolsasdeideas.springboot.web.app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
/***
 * En este controlador vamos a crear ejemplos para poder solicitar parametros desde la URL
 *  Request HTTP - GEt url
 * @author HP
 *
 */
@Controller
@RequestMapping("/params")
public class EjemploParamsController {
	/**
	 * 
	 * @param texto Nombre del parametro 
	 * @param model Model para el Timeleaf en los templates
	 * @return la vista a la que va asociada 
	 */
	@GetMapping("/")
	public String index(@RequestParam(name = "texto",required = false) String texto, Model model ) {
		
		model.addAttribute("resultado", "El resultado enviado es: " + texto);
		return "params/index";
	}
	
	/**
	 * Recordar que con el GetMapping vinculamos o mapeamos una ruta url con un metodo
	 * En el metodo vamos a solicitar un valor en la url llamado "texto" y este no es necesariamente requerido
	 * o podemos cambiarlo poniendo un valor por defecto y le decimos que es de tipo string
	 * 
	 * Recordar que el model es para utilizar Timeleaf en los templates
	 * ejemplo = http://localhost:8080/params/string?texto=hola que tal
	 * @param texto
	 * @param model
	 * @return
	 */
	
	@GetMapping("/string")
	public String param(@RequestParam(name = "texto",required = false) String texto, Model model ) {
		
		model.addAttribute("resultado", "El resultado enviado es: " + texto);
		return "params/ver";
	}
	/**
	 * Des esta manera podemos obtener multiple parametros a travez de la url de diferentes tipos
	 * @param saludo
	 * @param numero
	 * @param model
	 * @return
	 */
	@GetMapping("/mix-params")
	public String param(@RequestParam String saludo,@RequestParam Integer numero, Model model ) {
		
		model.addAttribute("resultado", "El saludo enviado es:  " + saludo+ "'y el numero es: '"+ numero+ "'");
		return "params/ver";
	}
	
	/**
	 * Este es otro metodo para recibir varios parametros utilizando la API ServletRequest 
	 * @param request
	 * @param model
	 * @return
	 */
	
	@GetMapping("/mix-params-request")
	public String param(HttpServletRequest request, Model model ) {
		String saludo = request.getParameter("saludo");
		Integer numero;
		try {
			numero = Integer.parseInt(request.getParameter("numero"));
		} catch (Exception e) {
			// TODO: handle exception
			numero = 0;
		}
		
		model.addAttribute("resultado", "El saludo enviado es:  " + saludo+ "'y el numero es: '"+ numero+ "'");
		return "params/ver";
	}
	
	

}
