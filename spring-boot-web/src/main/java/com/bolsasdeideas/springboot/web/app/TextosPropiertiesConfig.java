package com.bolsasdeideas.springboot.web.app;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
/**
 * En esta clase podremos obtener mas de dos properties indicando la direccion.
 * Lo que hacemos con application2 Es una inyeccion de dependencias en texto 
 * @author HP
 *
 */



@Configuration
@PropertySources({
	@PropertySource( "classpath:application2.properties")
	
})
public class TextosPropiertiesConfig {

}
