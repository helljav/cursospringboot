package com.bolsasdeideas.springboot.web.app.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bolsasdeideas.springboot.web.app.models.Usuario;


@Controller
@RequestMapping("/app") //Establecemos la ruta de primer nivel para poder acceder a los controladores
public class IndexController {
	@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;
	@Value("${texto.indexcontroller.perfil.titulo}")
	private String textoPerfil;
	@Value("${texto.indexcontroller.listar.titulo}")
	private String textoListar;
	
	/**
	 * Estemetodo esta ligado o mapeado a una cierta ruta 
	 * @param model te permite utilizar variables tymeleaf para utilizarlos en los templates 
	 * @return
	 */
	@GetMapping({"/index","/","","home"})
	public String index(Model model) {
		model.addAttribute("titulo", textoIndex);//Estas son variables que se utilizan en los templates
		return "index";		
	}

	
	@RequestMapping("/perfil")
	public String perfil(Model model) {
		Usuario usuario = new Usuario();
		usuario.setNombre("Javier");
		usuario.setApellido("Apellido");
		usuario.setEmail("franjavcp@outlook.com");
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", textoPerfil.concat(usuario.getNombre()));
		return "perfil";		
	}
	
	/**
	 * En ete metodo tenemos una lista de tipo usuario y la llenamos para que en el template "listar"
	 * Muestre a todos los usuarios con sus caracteristicas, este metodo esta mapeado al template url:
	 * localhost:8080/app/listar
	 * @param model
	 * @return
	 */
	@RequestMapping("/listar")
	public String listar(Model model) {
		
		model.addAttribute("titulo", textoListar);
		
		
		return "listar";		
	}
	
	/**
	 * Son metodos los cuales crean variables o valores en comun
	 * donde cualquier metodo lo puede ver y lo puede utilizar
	 * asi en sus templates que estan mapeados
	 * @return
	 */
	@ModelAttribute("usuarios")
	public ArrayList<Usuario> poblarUsuarios(){
		ArrayList<Usuario> usuarios = new ArrayList<>();
		usuarios.add(new Usuario ("Andres","Guszman", "andres@uncorreo.com"));
		usuarios.add(new Usuario ("Javier","Carrillo", "franjavcp@outlook.com"));
		usuarios.add(new Usuario ("Ivonne","Garcia", "ivonne@uncorreo.com"));
		
		return usuarios;
		
	}
}
