package com.bolsasdeideas.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Se encargara de redirigir al metodo indexcontroller
 * Con esto lo podemos hacer con redirect o forward, con el redirect se reinicia la peticion y formularios
 * Con el forward podremos mandar formularios
 * @author HP
 *
 */
@Controller
public class HomeController {
	
	@GetMapping("/")
	public String  home() {
		//Se recomienda hacer forward para las paginas propias 
		return "forward:/app/index";
	}

}
