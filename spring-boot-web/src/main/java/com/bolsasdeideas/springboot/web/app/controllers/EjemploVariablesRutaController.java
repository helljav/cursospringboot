package com.bolsasdeideas.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variables")
public class EjemploVariablesRutaController {
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("title","Enviar parametros de la ruta(@PathVariable)");
		return "variables/index";
		
	}
	//Una ruta que contiene una variable cualquiera que queramos enviar(parte dinamica)
	@GetMapping("/string/{texto}")
	/**
	 * Para este ejmplo de variables con ruta.
	 * El nombre de la variable que va a recibir en el metodo debe de llamarse igual 
	 * que la variable GetMapping o indicar el nombre con name = "texto"
	 * @param texto
	 * @return
	 */
	public String variable(@PathVariable String texto, Model model) {
		model.addAttribute("title","Recibir parametros de la ruta(@PathVariable)");
		model.addAttribute("resultado", "El texto enviado a la ruta es: "+ texto);
		return "variables/ver";
		
	}
	
	
	@GetMapping("/string/{texto}/{numero}")
	/**
	 * Para este ejmplo de variables con ruta.
	 * El nombre de la variable que va a recibir en el metodo debe de llamarse igual 
	 * que la variable GetMapping o indicar el nombre con name = "texto"
	 * @param texto
	 * @return
	 */
	public String variable(@PathVariable String texto,@PathVariable Integer numero, Model model) {
		model.addAttribute("title","Recibir parametros de la ruta(@PathVariable)");
		model.addAttribute("resultado", "El texto enviado a la ruta es: "+ texto +
				" Y el numero es: " + numero);
		return "variables/ver";
		
	}

}
