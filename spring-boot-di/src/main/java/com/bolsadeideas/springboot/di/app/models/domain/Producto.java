package com.bolsadeideas.springboot.di.app.models.domain;

import lombok.Data;

@Data
public class Producto {
	private String nombre;
	private Integer precio;
	public Producto(String nombre, Integer precio) {
	
		this.nombre = nombre;
		this.precio = precio;
	}
	
}
