package com.bolsadeideas.springboot.error.app.controllers;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * En este proyecto y en este controlador 
 * en espcifico; Vamos a personalizar y controlar los errores
 * como el 404, 500, errores algoritmicos, error de casteo, etc...
 * @author HP
 *
 */

//Control especial para los errores
@ControllerAdvice
public class ErrorHandlerController {
	
	
	@ExceptionHandler(ArithmeticException.class)
	public String aritmeticaError(Exception ex, Model model){
		//Añadimos los elementos al modelo para poderlo utilizar a la vista
		
		model.addAttribute("error", "Error de aritmetica");
		model.addAttribute("message", ex.getMessage());
		model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR);
		model.addAttribute("timestap", new Date());
		
		
		return "error/aritmetica";
	}
	
	
	@ExceptionHandler(NumberFormatException.class)
	public String numberFormatError(NumberFormatException ex, Model model) {
		
		//Añadimos cosas al model para la vista
		model.addAttribute("error", "Formato de numero invalido");
		model.addAttribute("message", ex.getMessage());
		model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR);
		model.addAttribute("timestap", new Date());
		
		return "error/numberError";
	}

}
