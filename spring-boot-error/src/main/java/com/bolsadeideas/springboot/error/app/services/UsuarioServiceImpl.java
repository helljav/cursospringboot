package com.bolsadeideas.springboot.error.app.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.error.app.models.domain.Usuario;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	private List<Usuario> lista;

	public UsuarioServiceImpl() {
		this.lista = new ArrayList<>();
		lista.add(new Usuario(1,"Javier", "Guzman"));
		lista.add(new Usuario(2,"Pep", "Garcia"));
		lista.add(new Usuario(3,"Yomi", "Lucero"));
		lista.add(new Usuario(4,"pac", "Gomez"));
		
	}

	@Override
	public List<Usuario> listar() {
		// TODO Auto-generated method stub
		return lista;
	}

	@Override
	public Usuario obtenerPorId(Integer id) {
		Usuario resultado = null;
		for(Usuario u: this.lista) {
			if(u.getId().equals(id)) {
				resultado = u;
				break;
			}
		}
		return resultado;
	}

}
